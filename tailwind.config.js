const colors = require('tailwindcss/colors');
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      backgroundColor: {
        primary: "var(--color-primary)",
        myGreen: "var(--color-green)",
      },
      textColor: {
        primary: "var(--color-primary)",
        myGreen: "var(--color-green)",
      },
      borderColor: {
        primary: "var(--color-primary)",
        myGreen: "var(--color-green)",
      },
      ringColor: {
        primary: "var(--color-primary)",
        myGreen: "var(--color-green)",
      },
      spacing: {
        21: '5.25rem',
      },
      colors: {
        gray: colors.coolGray,
        grayNormal: colors.gray,
        lightBlue: colors.lightBlue,
        black: {
          200: '#333',
          500: '#272727',
          600: '#222',
          700: '#1b1b1b',
          900: '#121212',
          0: '#000'
        },
      },
    },
    borderColor: theme => ({
      ...theme('colors'),
      black: {
        200: '#333',
        500: '#272727',
        600: '#222',
        700: '#1b1b1b',
        900: '#121212',
        0: '#000'
      }
    }),
    backgroundColor: theme => ({
      ...theme('colors'),
      black: {
        200: '#333',
        500: '#272727',
        600: '#222',
        700: '#1b1b1b',
        900: '#121212',
        0: '#000'
      }
    }),
    screens: {
      'xs': '480px',
      ...defaultTheme.screens,
    }
  },
  variants: {
    extend: {
      backgroundColor: ['checked'],
      borderWidth: ['first', 'last'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography')
  ],
}
