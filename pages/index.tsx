import Image from 'next/image';

export const Index = () => {
  return (
    <div className="flex flex-col min-h-screen">
      <main className="flex flex-col">
        <div className="xs:max-h-[235px] xs:h-[235px] md:max-h-[300px] md:h-[300px] flex flex-row overflow-hidden">
          <div className="xs:w-[135px] xs:min-w-[135px] md:w-[180px] md:min-w-[180px] xs:mr-4 md:mr-8 relative">
            <Image
              className="rounded-md"
              src={'https://ранобэ.рф/files/uploaded/books/136/78215ed3-6374-4964-a70c-8ed6b31035f2.bookLayout.jpg'}
              alt={'test'}
              width={180}
              height={250}
            />
          </div>
        </div>
      </main>
    </div>
  );
};

export default Index;